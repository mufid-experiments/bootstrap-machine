#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

cd
apt update
apt install -y zip unzip make g++ git
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install 10.19.0
git clone https://gitlab.com/mufid-experiments/theia.git
cd theia
mkdir -p ~/workspace
npm install --no-progress -g yarn
yarn install --no-progress
yarn theia build
