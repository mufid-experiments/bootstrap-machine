variable "do_token" {
  type        = string
  description = "DO API Token"
}

variable "total_servers" {
  type        = number
  description = "Target total server"
}

variable "ssh_public_key_name" {
  type        = string
  description = "Name of the Public key resource in Digital Ocean"
}

variable "domain_base" {
  type        = string
  description = "Base domain"
}

variable "ssh_public_key" {
  type        = string
  description = "Public key used to allow SSH access to the VM"
  default     = "~/.ssh/id_rsa.pub"
}

variable "ssh_private_key" {
  type        = string
  description = "Private key used to allow SSH access to the VM"
  default     = "~/.ssh/id_rsa"
}

variable "bastion_address" {
  type        = string
  description = "Address/range of bastion SSH"
  default     = "~/.ssh/id_rsa"
}
