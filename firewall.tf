
resource "digitalocean_firewall" "web" {
  name = "web-and-ssh"

  droplet_ids = digitalocean_droplet.testnet_droplet.*.id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.bastion_address]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
}
