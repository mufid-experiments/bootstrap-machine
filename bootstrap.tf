provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "ssh_key_skyia" {
  name       = "key_skyia"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "digitalocean_image" "bootstrap_initial" {
  name = "bootstrap-initial"
}

resource "digitalocean_droplet" "testnet_droplet" {
  count       = var.total_servers
  image       = data.digitalocean_image.bootstrap_initial.id
  name        = "test${count.index}.${var.domain_base}"
  region      = "sgp1"
  size        = "s-4vcpu-8gb"
  ipv6        = true
  resize_disk = false
  ssh_keys    = [digitalocean_ssh_key.ssh_key_skyia.fingerprint]
  connection {
    type = "ssh"
    user = "root"
    host = self.ipv4_address
    private_key = file("~/.ssh/id_rsa")
  }
}

resource "digitalocean_project" "bootstrap_project" {
  name    = "Bootstrap"
  description = "Bootstrap-related machine"
  purpose = "Projects"
  environment = "production"
  resources = digitalocean_droplet.testnet_droplet.*.urn
}

resource "digitalocean_domain" "bootstrap_skyandmurmur_com" {
  name       = var.domain_base
}

resource "digitalocean_record" "testn_bootstrap_skyandmurmur_com" {
  count  = var.total_servers
  domain = digitalocean_domain.bootstrap_skyandmurmur_com.name
  type   = "A"
  name   = "test${count.index}"
  ttl    = 60
  value  = digitalocean_droplet.testnet_droplet[count.index].ipv4_address
}

output "servers_created" {
  value = formatlist("%v.%v", digitalocean_record.testn_bootstrap_skyandmurmur_com.*.name, digitalocean_record.testn_bootstrap_skyandmurmur_com.*.domain)
}

output "domain_base" {
  value = var.domain_base
}
